#!/bin/bash

results_to_config(){
    for i in $(find DATA/results -maxdepth 1 -name *tr-1*.trim); do
	cp $i DATA/config
	echo $i
    done
    mmv  DATA/config/'*_tr-1_*.trim' DATA/'#1.alt.trim'
}

swap(){
    mmv DATA/config/'*.trim' DATA/config/'#1.alt.trim'
    mmv DATA/'*.alt.trim' DATA/config/'#1.trim'
    mv DATA/config/*alt.trim DATA/
}

results_to_config
swap




