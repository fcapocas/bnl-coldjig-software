#map of AMAC register to funtions
#(register,offset bit)
amac_read_registers={
    "master_status":(0,0),
    "i2c_address":(0,4),
    
    "left_temperature":((25,0),(26,0)),
    "right_temperature":((34,0),(35,0)),
    
    "left_current":((27,4),(28,0)),
    "right_current":((36,4),(37,0)),
    
    "status_hv_ilock":(0,0),
    "status_lv_Ilock":(0,1),
    "status_hv_warn":(0,2),
    "status_hv_warn":(0,3),

    "status_id":(0,4),
    "hv_enable":(43,0),
    "lv_enable":(44,0),
    "set_bandgap":(40,0),

    "left_vbandgap":((22,4),(23,0)),
    "right_Vbadgap":((31,4),(32,0)),
    
    "left_VDD_hybrid":((23,6),(24,0)),
    "right_VDD_hybrid":((32,6),(33,0)),
    
    "left_OTA":((26,2),(27,0)),
    "right_OTA":((35,2),(36,0)),
    
    "left_vdd":((20,0),(21,0)),
    "right_vdd":((29,0),(30,0)),
    "left_vchan":((21,2),(22,0)),
    "right_vchan":((30,2),(31,0))
}


amac_write_registers={
    "bandgap":(40,0),
    "lv_enable":(44,0),
    "hv_enable":(43,0),
    "hv_freq":(43,1),
    "left_ramp_gain":(45,0),
    "left_ch0_select":(45,2),
    "left_ch3_select":(45,3),
    "right_ramp_gain":(45,4),
    "right_ch0_select":(45,5),
    "right_ch3_select":(45,6),
    "left_opamp_gain":(42,0),
    "right_opamp_gain":(42,4),
    "right_ch0_select":(45,6)
}
