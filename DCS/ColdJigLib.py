from __future__ import print_function
import time

import threading
try:
    import thread
except ImportError:
    import _thread
from zmq_client import Client
import re
import sht_sensor
try:
    from queue import Queue as queue
except ImportError:
    from Queue import Queue as queue
import DAQListner
from DCSTools import message_parser

from collections import deque
import numpy as np
from db_manager import db_manager

def check_stability(temperature_deque):
    tolerance=0.2
    return np.std(temperature_deque)<tolerance
#local configuration
# initilaize uses the standard INI file parser but the syntax is
# different for Python 2.X and Python 3.x.  This if block imports the
# correct version based on the Python Version running.
import sys
if (sys.version_info > (2,8)) :
    # Import the Python 3 version of initialize
    from initialize import SensorDict
    from initialize import master,grafana
    from initialize import dcs_dict
    from initialize import IVCurveConfig
else :
    # Import the Python 2.x version of initialize
    from initialize_2 import SensorDict
    from initialize_2 import master,grafana
    from initialize_2 import dcs_dict
    from initialize_2 import IVCurveConfig


print("dcs_dict")
print(dcs_dict)
import HVTools
database=db_manager("influx",database="FourModuleBox")
DAQ_client=Client(master,"5555")
database.init("http://"+grafana)
CommQueue= queue()
CycleQueue=queue()
Chiller=dcs_dict['Chiller']
lock = threading.Lock()
kill_button= threading.Event()
kill_button.set()
DAQListner.start()


def panic():
    kill_button.clear()
    LV_TurnOff()
    HV_TurnOff()
    Chiller_TurnOff()
    
def Ping():
    return DAQ_client.Ping()
def start_sensors():
    CommQueue.queue.clear()
    sensor_thread=SensorThread("sensor thread",CommQueue,LV=dcs_dict['LV'],HV=dcs_dict['HV'],chiller=dcs_dict['Chiller'])
    sensor_thread.start()

def stop_sensors():
    CommQueue.put("break")
                
def do_IVcurve():
    #run IV curve test in seperate thread
    step,end,start=IVCurveConfig()
    thread.start_new_thread(HVTools.IVCurve, (dcs_dict["HV"],grafana,step,end,start,))
 
def LV_TurnOn():
    try:
        dcs_dict['LV'].TurnOn(1)
        dcs_dict['LV'].TurnOn(2)
        return True
    except:
        return False

def LV_TurnOff():
    try:
        dcs_dict['LV'].TurnOff(1)
        dcs_dict['LV'].TurnOff(2)
        return True
    except:
        return False

def LV_SetVoltage(value,channel):
    try:
        dcs_dict['LV'].SetVoltage(value,channel)
        return True
    except:
        return False
    
def LV_SetCurrent(value,channel):
    try:
        dcs_dict['LV'].SetCurrent(value,channel)
        return True
    except:
        return False
    
def LV_SetVoltageProtection(value,channel):
    try:
        dcs_dict['LV'].SetVoltageProtection(value,channel)
        return True
    except:
        return False
    

def LV_GetCurrent(channel):
    try:
        current=dcs_dict['LV'].GetActualCurrent(channel)
        return current
    except:
        return None
    
def LV_GetVoltage(channel):
    try:
        voltage=dcs_dict['LV'].GetVoltage(channel)
        return voltage
    except:
        return None
    
def HV_TurnOn():
    try:
        dcs_dict['HV'].TurnOn()
        return True
    except:
        return None
def HV_TurnOff():
    try:
        dcs_dict['HV'].TurnOff()
        return True
    except:
        return None
    
def HV_SetCompliance(value):
    try:
        dcs_dict['HV'].SetCurrentCompliance(value)
        return True
    except:
        return None

def HV_SetRange(voltage_range):
    try:
        dcs_dict['HV'].SetVoltageRange(float(voltage_range))
        return True
    except:
        return None
    
def HV_SetVoltage(value):
    try:
        with lock:
            dcs_dict['HV'].SetVoltageLevel(float(value))
        return True
    except Exception as e:
        print(e)
        return False
    
def HV_SetCurrent(value):
    try:
        dcs_dict['HV'].SetCurrentLevel(value)
        return True
    except:
        return None
    
def HV_GetVoltage():
    try:
        current=dcs_dict['HV'].GetVoltage()
        return current
    except:
        return None
def HV_GetCurrent():
    try:
        current=dcs_dict['HV'].GetCurrent()
        return current
    except:
        return None

def Chiller_TurnOn():
    try:
        dcs_dict['Chiller'].TurnOn()
        return True
    except TypeError:
        return False


def Chiller_TurnOff():
    try:
        dcs_dict['Chiller'].TurnOff()
        return True
    except TypeError:
        return False


def Chiller_SetTemperature(value):
    try:    
        dcs_dict['Chiller'].SetTemperature(value)
        return True
    except TypeError:
        return False

def Chiller_GetTemperature():
    temp=dcs_dict['Chiller'].GetTemperature()
    return temp
#return None

def do_trims(msg):
    msg_data=msg.split()
    msg_data[3]="true"
    msg_data[4]="true"
    return " ".join(msg_data)


def Start_ThermalCycle(message,lower_temp,upper_temp):
    global DAQ_client
    global CycleQueue
    #message is the tests to run
    sensor=SensorDict["NTC_1"]
    print("coldjiglib start thermal cycle")
    cycler=ThermalCycle("Thermal Cycle",CycleQueue,DAQ_client,sensor,dcs_dict['Chiller'],message,target_temperature=int(lower_temp),room_temperature=int(upper_temp))
    cycler.start()
    
def Thermal_Cycle(command):
    global CycleQueue
    print("changed cycle")
    CycleQueue.put(command)
    
class SensorThread(threading.Thread):
    def __init__(self,threadID,comm_queue,interlock=None,LV=None,HV=None,chiller=None):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.queue=comm_queue
        self.interlock=interlock
        self.LV=LV
        self.HV=HV
        #self.Chiller=chiller
        print("Granafa Server:",grafana)
    

    def run(self):
        print ("Starting " + str(self.threadID))
        self.Read()

    def Read(self):
        command=""
        while kill_button.is_set():
            if (not self.queue.empty()):
                command=str(self.queue.get())
                print("from queue", command)
                if 'break' in command.lower():
                    break
            with lock:
                try:
                    lv_voltage=self.LV.GetVoltage(1)
                    lv_current=self.LV.GetActualCurrent(1)
                    print(type(lv_voltage))
                    print(float(str(lv_voltage)))
                    database.send("QCBox.LVVoltage",float(lv_voltage))
                    database.send("QCBox.LVCurrent",float(lv_current))
                except Exception as e:
                    print(str(e))
                    print("low voltage not responding")
            """with lock:
                try:
                    hv_voltage=dcs_dict['HV'].GetVoltage()
                    hv_current=dcs_dict['HV'].GetCurrent()
                    print(type(hv_voltage))
                    print(float(str(hv_voltage)))
                    database.send("QCBox.HVVoltage",float(hv_voltage))
                    database.send("QCBox.HVCurrent",float(hv_current))
                except Exception as e:
                    print(str(e))
                    print("high voltage not responding")"""
            with lock:
                try:
                    temp=Chiller_GetTemperature()
                    print(float(str(temp)))
                    database.send("QCBox.ChillerTemperature",float(temp))
                except Exception as e:
                    print(str(e))
                    print("chiller not responding")
                                                                    
            with lock:
                try:
                    data=self.interlock.get_data()
                    data=data.split(",")
                    metrics=["hybrid NTC 1",
                             "hybrid NTC 2",
                             "hybrid NTC 3",
                             "hybrid NTC 4",
                             "hybrid NTC 5",
                             "hybrid NTC 6",
                             "hybrid NTC 7",
                             "hybrid NTC 8",
                             "user NTC 1",
                             "user NTC 2",
                             "SHT_temp",
                             "SHT_humidity"]
                             #"HAFFlow 1"]
                    
                    for point,metric in zip(data,metrics):
                        point=int(point)
                        name="interlock."+metric
                        database.send(name,point)
                except AttributeError:
                    print("interlock not responding")
                    
            for label,sensor in SensorDict.items():
                try:
                    sensor_items=sensor.get_data().items()
                except Exception as e:
                    print("problem with "+label)
                    print(e)
                    continue
                for name,data in sensor_items:
                    try:
                        grafana_name='QCBox.'+str(label)
                        grafana_name+="."+name
                        print(grafana_name,data)
                        if 'humid' in name.lower():  #attempt at an interlock on humidty
                            if (data > 10):
                                try:
                                    HV_TurnOff()
                                    print("Turned off high voltage because of humidity")
                                except:
                                    print("tried and failed to turn off high voltage because of high humidity")
                                try:
                                    LV_TurnOff()
                                    print("turned off low voltage because of high humidty")
                                except:
                                    print("tried and failed to turn off low voltage because of high humidity")
                        database.send(grafana_name,float(data))
                    except (sht_sensor.sensor.ShtCRCCheckError,sht_sensor.sensor.ShtCommFailure):
                        print("SHT sensor in config file but not connected properly")
                    except AttributeError:
                        print("encountered a problem with "+label)
            time.sleep(0.5)
        

class ThermalCycle(threading.Thread):
    def __init__(self,threadID,comm_queue,DAQ_client,sensor,Chiller,message,total_cycles=10,target_temperature=15,room_temperature=20):
        threading.Thread.__init__(self)
        self.threadID = threadID
       # self.chiller = Chiller
        self.queue=comm_queue
        self.DAQ_client=DAQ_client
        print("DAQ host::")
        print(DAQ_client.host)
        self.target_temperature=target_temperature
        self.room_temperature=room_temperature
        self.total_cycles=total_cycles
        self.sensor=sensor
        self.message=message
        try:
            with lock:
                Chiller_SetTemperature(int(self.room_temperature)+5)
                print("set chiller to ", int(self.room_temperature)+5)
        except Exception as e:
            print("could not set chiller")
            print(e)
            
    def run(self):
        print ("Starting " + str(self.threadID))
        self.loop()
        
        
    def loop(self):
        print("STARTED THERMAL CYCLE THREAD","UPPER TEMP= ",self.room_temperature,"LOWER BOUND= ",self.target_temperature)
        Cycle='High'
        number=0
        finished_cycle=False
        msg=''
        temperature_deque=deque()
        while number<self.total_cycles and kill_button.is_set():
            if number==0:
                msg=do_trims(self.message)
            else:
                msg=self.message
            time.sleep(0.5)
            try:
                temp=self.sensor.get_data()['temperature']
            except Exception as e:
                print("could not read temperature")
                print(e)
                temp=self.room_temperature
            database.send("QCBox.ThermalCycle",temp)
            temperature_deque.append(temp)
            if len(temperature_deque)>4:
                temperature_deque.popleft()

            print("thermal cycle temp",temp,finished_cycle)
            if (not self.queue.empty()):
                command=str(self.queue.get())
                print("from queue", command)
                if 'break' in command.lower():
                    print("ending thermal cycle")
                    break
                if 'change_temp' in command.lower():
                    if Cycle=='High' and temp >= self.room_temperature:
                        tries=0
                        while tries <3:
                            try:
                                with lock:
                                    Chiller_SetTemperature(int(self.target_temperature)-10)
                                    print("chiller set to ",int(self.target_temperature)-10)
                                    Cycle='Low'
                                    finished_cycle=False
                                    time.sleep(60)
                                    break
                            except Exception as e:
                                print("failed to set chiller , trying "+str(3-tries)+" more times")
                                print(e)
                                time.sleep(0.5)
                                tries+=1
                                continue
                        if (tries==3):
                            print("lost chiller communication")
                            break
                        finished_cycle=False
                    if Cycle=='Low' and temp <=self.target_temperature:
                        tries=0
                        while tries < 3:
                            try:
                                with lock:
                                    Chiller_SetTemperature(int(self.room_temperature)+5)
                                    print("chiller set to",self.room_temperature+5)
                                    Cycle='High'
                                    number+=1
                                    finished_cycle=False
                                    time.sleep(60)
                                    break
                            except Exception as e:
                                print("failed to set chiller , trying "+str(3-tries)+" more times")
                                print(e)
                                tries+=1
                                time.sleep(0.5)
                                continue
                        if tries==3:
                            print("lost chiller communication")
                            break
            else:
                if Cycle=='High' and temp >= self.room_temperature and not finished_cycle and check_stability(temperature_deque):
                    print("sending server to do itsdaq test at high")
                    print(temp)
                    finished_cycle=True
                    print("tests", msg)
                    DAQ_client.SendServerMessage(msg)
                    
                elif Cycle=='Low' and temp <= self.target_temperature and not finished_cycle and check_stability(temperature_deque):
                    finished_cycle=True
                    print("sending server to do itsdaq test at low")
                    print(temp)
                    print("tests", msg)
                    DAQ_client.SendServerMessage(msg)
        #finished cycle do hv measurements for 2 hours:
        t_end = time.time() + 60 * 120
        """while time.time() < t_end:
            time.sleep(20)
            with lock:
                try:
                    hv_current=dcs_dict['HV'].GetCurrent()
                    print("high voltage stability test"+str(dcs_dict['HV'].GetVoltage))
                    database.send("QCBox.ThermalCycle.HVCurrent",float(hv_current))
                except Exception as e:
                    print(str(e))
                print("high voltage not responding")"""
        print("finished_cycles")
        LV_TurnOff()
        HV_TurnOff()
        Chiller_TurnOff()
        print("Turned off equipment")
            
