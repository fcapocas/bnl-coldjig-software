from Sensors.HAFFlow import HAFFlow
from Sensors.HH309A import HH309A
from Sensors.HIH import HIH
from Sensors.SHT75 import SHT75
from Sensors.SHT85 import SHT85
from Sensors.NTC import NTC
from Sensors.AIR import AIR
try:
    from Sensors.ThermoCouple import ThermoCouple
except:
    print("not using TC08 thermocouple reader")
from Sensors.Sensor import Sensor
from Sensors.hscsip import hscsip
