import time
import piplates.DAQCplate as DAQ
import numpy as np
from Sensors.Sensor import Sensor
class FlowSensor(Sensor):
        def __init__(self,input_pin=1,flow_range=5,mode='SLPM'):
                Sensor.__init__(self,"AWM5101")
                self.input_pin=input_pin
                self.mode=mode
                self.range=flow_range
                self.b=1
                self.m=4.0/flow_range

        def GetFlow(self):
                Vin=DAQ.getADC(0,self.input_pin)
                flow=(Vin-self.b)/self.m
                if(self.mode == 'SLPM'):
                        return flow
                elif(self.mode=='SCFH'):
                        return 60.0/28.3*flow
                else:
                        print("non implemented unit "+self.mode)
                        return 0

        def GetRaw(self):
                return DAQ.getADC(0,self.input_pin)

        def get_data():
                self.data={"Flow_rate":GetFlow}
                return self.data
